import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:stockspro/domain/stocks/search_list_item.dart';
import 'package:stockspro/remote/constants.dart';

class SearchRepository {

  Future<List<SearchListItem>> search(String name) async {
    try {
      var queryParameters = {
        'query': '$name',
        'limit': '10',
        'exchange': 'NASDAQ'
      };
      var searchUri = Uri.https('financialmodelingprep.com',
          '/api/v3/search', queryParameters);
      final response = await http.get(searchUri);
      if (response.statusCode == 200) {
        var stocksListResponse = json.decode(response.body);
        //var stocksListJson = stocksListResponse[''];
        var stocksList = List<SearchListItem>();

        stocksListResponse.forEach((dynamic val) {
          var record = SearchListItem(symbol: val['symbol'], name: val['name'],
              currency: val['currency'], stockExchange: val['stockExchange'],
              exchangeShortName: val['exchangeShortName']);
          stocksList.add(record);
        });

        return stocksList;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}
