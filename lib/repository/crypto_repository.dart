import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:stockspro/domain/crypto/crypto_list_item.dart';
import 'package:stockspro/remote/constants.dart';

class CryptoRepository {

  Future<List<CryptoListItem>> loadStocksListWithRealTimePrice() async {
    try {
      final response = await http.get('${BASE_URL}cryptocurrencies');
      if (response.statusCode == 200) {
        var cryptoListResponse = json.decode(response.body);
        var cryptoListJson = cryptoListResponse['cryptocurrenciesList'];
        var cryptoList = List<CryptoListItem>();

        cryptoListJson.forEach((dynamic val) {
          var record = CryptoListItem(ticker: val['ticker'], name: val['name'],
              price: val['price'], changes: val['changes'],
              marketCapitalization: val['marketCapitalization']);
          cryptoList.add(record);
        });

        return cryptoList;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }


}
