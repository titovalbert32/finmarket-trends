import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:stockspro/domain/stocks/most_value_stock.dart';
import 'package:stockspro/domain/stocks/stock_list_item.dart';
import 'package:stockspro/remote/constants.dart';

class StocksRepository {

  Future<List<StockListItem>> loadStocksListWithRealTimePrice() async {
    try {
      final response = await http.get('${BASE_URL}stock/real-time-price');
      if (response.statusCode == 200) {
        var stocksListResponse = json.decode(response.body);
        var stocksListJson = stocksListResponse['stockList'];
        var stocksList = List<StockListItem>();

        stocksListJson.forEach((dynamic val) {
          var record = StockListItem(symbol: val['symbol'], price: val['price']);
          stocksList.add(record);
        });

        return stocksList;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<List<MostValueStock>> loadMostGainers() async {
    try {
      final response = await http.get('${BASE_URL}stock/gainers');
      if (response.statusCode == 200) {
        var gainerStocksResponse = json.decode(response.body);
        var gainerStocksListJson = gainerStocksResponse['mostGainerStock'];
        var gainerStocksList = List<MostValueStock>();

        gainerStocksListJson.forEach((dynamic val) {
          var record = MostValueStock(
              ticker: val['ticker'],
              changes: val['changes'],
              price: val['price'],
              changesPercentage: val['changesPercentage'],
              companyName: val['companyName']);
          gainerStocksList.add(record);
        });

        return gainerStocksList;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<List<MostValueStock>> loadMostLosers() async {
    try {
      final response = await http.get('${BASE_URL}stock/losers');
      if (response.statusCode == 200) {
        var loserStocksResponse = json.decode(response.body);
        var loserStocksListJson = loserStocksResponse['mostLoserStock'];
        var loserStocksList = List<MostValueStock>();

        loserStocksListJson.forEach((dynamic val) {
          var record = MostValueStock(
              ticker: val['ticker'],
              changes: val['changes'],
              price: val['price'],
              changesPercentage: val['changesPercentage'],
              companyName: val['companyName']);
          loserStocksList.add(record);
        });

        return loserStocksList;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<List<MostValueStock>> loadMostActive() async {
    try {
      final response = await http.get('${BASE_URL}stock/actives');
      if (response.statusCode == 200) {
        var activeStocksResponse = json.decode(response.body);
        var activeStocksListJson = activeStocksResponse['mostActiveStock'];
        var activeStocksList = List<MostValueStock>();

        activeStocksListJson.forEach((dynamic val) {
          var record = MostValueStock(
              ticker: val['ticker'],
              changes: val['changes'],
              price: val['price'],
              changesPercentage: val['changesPercentage'],
              companyName: val['companyName']);
          activeStocksList.add(record);
        });

        return activeStocksList;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}
