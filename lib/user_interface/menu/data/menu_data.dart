import 'package:flutter/material.dart';
import 'package:stockspro/user_interface/styles/my_flutter_app_icons.dart';

class Destination {
  const Destination(this.title, this.icon, this.color);
  final String title;
  final IconData icon;
  final MaterialColor color;
}

const List<Destination> allDestinations = <Destination>[
  Destination(STOCKS, Icons.trending_up, Colors.teal),
  Destination(CRYPTO, MyFlutterApp.bitcoin, Colors.cyan),
  Destination(FOREX, MyFlutterApp.money, Colors.orange),
  Destination(NEWS, MyFlutterApp.newspaper, Colors.blue)
];

const String STOCKS = "Stocks";
const String CRYPTO = "Crypto";
const String FOREX = "Forex";
const String NEWS = "News";