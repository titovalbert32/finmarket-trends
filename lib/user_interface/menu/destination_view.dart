import 'package:flutter/material.dart';
import 'package:stockspro/user_interface/menu/data/menu_data.dart';
import 'package:stockspro/user_interface/screens/crypto/crypto_list.dart';
import 'package:stockspro/user_interface/screens/stocks/stocks_list.dart';
import 'package:stockspro/user_interface/screens/stocks/gainers_list.dart';
import 'package:stockspro/user_interface/screens/stocks/active_list.dart';
import 'package:stockspro/user_interface/screens/stocks/losers_list.dart';
import 'package:stockspro/user_interface/screens/stocks/data/stocks_list_data.dart';

class DestinationView extends StatefulWidget {
  const DestinationView({ Key key, this.destination }) : super(key: key);
  final Destination destination;
  @override
  _DestinationViewState createState() => _DestinationViewState(destination: destination);
}

class _DestinationViewState extends State<DestinationView> {

  final Destination destination;
  _DestinationViewState({ this.destination });

  @override
  Widget build(BuildContext context) {
    return Navigator(
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) {
            switch(settings.name) {
              case '/':
                switch (destination.title) {
                  case STOCKS : return StocksList();
                  case CRYPTO : return CryptoList();
                }
                return StocksList();
              case ACTIVE:
                return ActiveList();
              case GAINERS:
                return GainersList(destination: destination);
              case LOSERS:
                return LosersList();
              default:
                return StocksList();
            }
          },
        );
      },
    );
  }
}