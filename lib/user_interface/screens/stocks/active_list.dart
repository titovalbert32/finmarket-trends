import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stockspro/domain/stocks/most_value_stock.dart';
import 'package:stockspro/repository/stocks_repository.dart';

class ActiveList extends StatefulWidget {
  @override
  _ActiveListState createState() => _ActiveListState();
}

class _ActiveListState extends State<ActiveList> {
  StocksRepository repository = StocksRepository();

  List<MostValueStock> data = [];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SizedBox.expand(
        child: ListView(
          children: _buildList(),
        ),
      ),
    );
  }

  List<Widget> _buildList() {

    return data
        .map((MostValueStock stock) => Card(
              child: ListTile(
                title: Container(child: Text(stock.ticker), padding: EdgeInsets.only(top: 8)),
                subtitle: Container(child: Text(stock.companyName), padding: EdgeInsets.only(bottom: 8)),
                //leading: CircleAvatar(child: Text(f.rank.toString())),
                trailing: Container(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Column(
                    children: <Widget>[
                      Text(stock.price,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blueGrey,
                              fontSize: 16)),
                      Text(
                          '${stock.changes.toStringAsFixed(2)} ${stock.changesPercentage}',
                          style: TextStyle(color: stock.changes < 0? Colors.red : Colors.green))
                    ],
                  ),
                ),
              ),
            ))
        .toList();
  }

  _initGainersList() async {
    List<MostValueStock> mostActive = await repository.loadMostActive();
    setState(() {
      data = mostActive;
    });
  }

  @override
  void initState() {
    super.initState();
    _initGainersList();
  }
}
