import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stockspro/domain/stocks/search_list_item.dart';
import 'package:stockspro/repository/search_repository.dart';
import 'package:stockspro/user_interface/screens/stocks/data/stocks_list_data.dart';

class StocksList extends StatefulWidget {
  @override
  _StocksListState createState() => _StocksListState();
}

class _StocksListState extends State<StocksList> {
  SearchRepository repository = SearchRepository();

  List<SearchListItem> searchList = [];
  List<StocksMenuItem> data = stocksMenu;

  TextEditingController editingController = TextEditingController();

  searchStock(String name) async {
    var searchItems = name.isEmpty ? List<SearchListItem>() : await repository.search(name);
    setState(() {
      searchList = searchItems;
    });
  }

  List<Widget> _buildSearchList() {
    return searchList
        .map((SearchListItem item) => Card(
      child: ListTile(
        title: Text(item.symbol != null ? item.symbol : ''),
        subtitle: Text(item.name != null ? item.name : ''),
        //leading: CircleAvatar(child: Text(f.rank.toString())),
        /*trailing: Container(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Column(
            children: <Widget>[
              Text(item.price,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey,
                      fontSize: 16)),
              Text(
                  '${gainerStock.changes.toStringAsFixed(2)} ${gainerStock.changesPercentage}',
                  style: TextStyle(color: Colors.green))
            ],
          ),
        ),*/
      ),
    ))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 32, left: 8, right: 8),
          child: TextField(
            onChanged: (value) {
              searchStock(value);
            },
            controller: editingController,
            decoration: InputDecoration(
                labelText: "Search by stock's name",
                hintText: "Search",
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8.0)))),
          ),
        ),
        Expanded(
          child: ListView(
            children: _buildSearchList(),
          ),
        ),
        Expanded(
            child: ListView.builder(
              itemCount: data.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                  elevation: 2,
                  margin: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                  child: Container(
                    child: ListTile(
                      leading: Container(
                        padding: EdgeInsets.only(right: 6),
                        child: Icon(data[index].icon,
                            color: Theme.of(context).textTheme.title.color),
                        decoration: BoxDecoration(
                          border: Border(
                              right: BorderSide(width: 1, color: Colors.white24)),
                        ),
                      ),
                      contentPadding:
                      EdgeInsets.symmetric(horizontal: 16, vertical: 2),
                      title: Text(
                        data[index].title,
                        style: TextStyle(
                            color: Theme.of(context).textTheme.title.color,
                            fontWeight: FontWeight.bold),
                      ),
                      trailing: Icon(Icons.keyboard_arrow_right,
                          color: Theme.of(context).textTheme.title.color),
                      subtitle: Text(data[index].description),
                      onTap: () {
                        Navigator.pushNamed(context, data[index].title);
                      },
                    ),
                  ),
                );
              },
            )
        ),
      ],
    );
  }
}
