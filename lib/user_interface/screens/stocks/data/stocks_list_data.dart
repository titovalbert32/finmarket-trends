import 'package:flutter/material.dart';

class StocksMenuItem {
  const StocksMenuItem(this.title, this.description, this.icon, this.color);
  final String title;
  final String description;
  final IconData icon;
  final MaterialColor color;
}

const List<StocksMenuItem> stocksMenu = <StocksMenuItem>[
  StocksMenuItem(ACTIVE, 'Most active stock companies.', Icons.settings_input_component, Colors.teal),
  StocksMenuItem(GAINERS, 'Most gainer stock companies.', Icons.trending_up, Colors.cyan),
  StocksMenuItem(LOSERS, 'Most losers stock companies.', Icons.trending_down, Colors.orange),
];

const String ACTIVE = "Active";
const String GAINERS = "Gainers";
const String LOSERS = "Losers";