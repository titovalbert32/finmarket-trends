import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stockspro/domain/stocks/most_value_stock.dart';
import 'package:stockspro/repository/stocks_repository.dart';

class LosersList extends StatefulWidget {
  @override
  _LosersListState createState() => _LosersListState();
}

class _LosersListState extends State<LosersList> {
  StocksRepository repository = StocksRepository();

  List<MostValueStock> data = [];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SizedBox.expand(
        child: ListView(
          children: _buildList(),
        ),
      ),
    );
  }

  List<Widget> _buildList() {
    return data
        .map((MostValueStock gainerStock) => Card(
              child: ListTile(
                title: Text(gainerStock.ticker),
                subtitle: Text(gainerStock.companyName),
                //leading: CircleAvatar(child: Text(f.rank.toString())),
                trailing: Container(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Column(
                    children: <Widget>[
                      Text(gainerStock.price,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blueGrey,
                              fontSize: 16)),
                      Text(
                          '${gainerStock.changes.toStringAsFixed(2)} ${gainerStock.changesPercentage}',
                          style: TextStyle(color: Colors.red))
                    ],
                  ),
                ),
              ),
            ))
        .toList();
  }

  _initGainersList() async {
    List<MostValueStock> mostLosers = await repository.loadMostLosers();
    setState(() {
      data = mostLosers;
    });
  }

  @override
  void initState() {
    super.initState();
    _initGainersList();
  }
}
