import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stockspro/domain/stocks/most_value_stock.dart';
import 'package:stockspro/repository/stocks_repository.dart';
import 'package:stockspro/user_interface/menu/data/menu_data.dart';

class GainersList extends StatefulWidget {

  final Destination destination;
  GainersList({ this.destination });

  @override
  _GainersListState createState() => _GainersListState();
}

class _GainersListState extends State<GainersList> {

  StocksRepository repository = StocksRepository();

  List<MostValueStock> data = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Most gainers'),
        backgroundColor: widget.destination.color,
    /*actions: <Widget>[
    SaveButton(onPressed: _saveWorkout)
    ],*/
    ),
      body: Container(
      child: SizedBox.expand(
        child: ListView(
          children: _buildList(),
        ),
      ),
    ),);
  }

  List<Widget> _buildList() {
    return data
        .map((MostValueStock gainerStock) => Card(
              child: ListTile(
                title: Text(gainerStock.ticker),
                subtitle: Text(gainerStock.companyName),
                //leading: CircleAvatar(child: Text(f.rank.toString())),
                trailing: Container(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Column(
                    children: <Widget>[
                      Text(gainerStock.price,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blueGrey,
                              fontSize: 16)),
                      Text(
                          '${gainerStock.changes.toStringAsFixed(2)} ${gainerStock.changesPercentage}',
                          style: TextStyle(color: Colors.green))
                    ],
                  ),
                ),
              ),
            ))
        .toList();
  }

  _initGainersList() async {
    List<MostValueStock> mostGainers = await repository.loadMostGainers();
    setState(() {
      data = mostGainers;
    });
  }

  @override
  void initState() {
    super.initState();
    _initGainersList();
  }
}
