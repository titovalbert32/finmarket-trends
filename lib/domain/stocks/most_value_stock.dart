
class MostValueStock {

  MostValueStock({ this.ticker, this.changes, this.price, this.changesPercentage, this.companyName });

  String ticker;
  double changes;
  String price;
  String changesPercentage;
  String companyName;
}