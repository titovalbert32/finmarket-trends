
class StockListItem {

  StockListItem({ this.symbol, this.price });

  String symbol;
  double price;
}