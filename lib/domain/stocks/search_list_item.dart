
class SearchListItem {

  SearchListItem({ this.symbol, this.name, this.currency, this.stockExchange, this.exchangeShortName });

  String symbol;
  String name;
  String currency;
  String stockExchange;
  String exchangeShortName;
}