
class CryptoListItem {

  CryptoListItem({ this.ticker, this.name, this.price, this.changes, this.marketCapitalization });

  String ticker;
  String name;
  double price;
  double changes;
  int marketCapitalization;
}